# Subtitles Translater

## Résumé

Script NodeJS utilisant [DeepL](https://www.deepl.com/translator) pour traduire des fichiers de sous-titres au format `.srt` automatiquement.

## Usage 

```powershell
USAGE : node index.js subtitlesFile.srt [sourceLang] [targetLang]

sourceLang : default value = "EN-US"
targetLang : default value = "FR"
```
